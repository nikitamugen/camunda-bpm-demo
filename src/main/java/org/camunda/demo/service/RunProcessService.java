package org.camunda.demo.service;

import java.util.Map;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RunProcessService {

	private final Logger logger = LoggerFactory.getLogger(RunProcessService.class);
	
	@Autowired
	private CamelContext camelContext;

	public void run(String processId, Map<String, String> variables) {
		
		logger.info("RUN PROCESS: {} with variables: {}", processId, variables);
		
		ProducerTemplate producer = camelContext.createProducerTemplate();
		producer.sendBodyAndHeader("direct:start-process", variables, "PROCESS_ID", processId);
	}
}
