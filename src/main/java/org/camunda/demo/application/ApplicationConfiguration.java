package org.camunda.demo.application;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.spring.ProcessEngineFactoryBean;
import org.camunda.bpm.engine.spring.SpringProcessEngineConfiguration;
import org.camunda.bpm.engine.spring.SpringProcessEngineServicesConfiguration;
import org.camunda.demo.route.DemoRoute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@Import(SpringProcessEngineServicesConfiguration.class)
public class ApplicationConfiguration {

	private Logger logger = LoggerFactory.getLogger(ApplicationConfiguration.class.getName());
	
	@Value("${camunda.bpm.history-level:none}")
	private String historyLevel;

	// add more configuration here
	// ---------------------------

	// configure data source via application.properties
	@Autowired
	private DataSource dataSource;

	@Autowired
	private ResourcePatternResolver resourceLoader;

	@Bean
	public SpringProcessEngineConfiguration processEngineConfiguration() throws IOException {
		SpringProcessEngineConfiguration config = new SpringProcessEngineConfiguration();

		config.setDataSource(dataSource);
		config.setDatabaseSchemaUpdate("true");

		config.setTransactionManager(transactionManager());

		config.setHistory(historyLevel);

		config.setJobExecutorActivate(true);
		config.setMetricsEnabled(false);

		// deploy all processes from folder 'processes'
		Resource[] resources = resourceLoader.getResources("classpath:/bpmn/*.bpmn");
		config.setDeploymentResources(resources);

		return config;
	}

	@Bean
	public PlatformTransactionManager transactionManager() {
		return new DataSourceTransactionManager(dataSource);
	}

	@Bean
	public ProcessEngineFactoryBean processEngine() throws IOException {
		ProcessEngineFactoryBean factoryBean = new ProcessEngineFactoryBean();
		factoryBean.setProcessEngineConfiguration(processEngineConfiguration());
		return factoryBean;
	}
}
