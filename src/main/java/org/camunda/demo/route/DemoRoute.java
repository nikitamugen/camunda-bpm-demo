package org.camunda.demo.route;

import java.util.HashMap;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component("DemoRoute")
public class DemoRoute extends RouteBuilder {

	private Logger logger = LoggerFactory.getLogger(DemoRoute.class.getName());
	
	@Override
	public void configure() throws Exception {
		
		from("direct:start-process")
			.log("Start process: ${header.PROCESS_ID}!")
			.toD("camunda-bpm://start?processDefinitionKey=${header.PROCESS_ID}");
		
		from("direct:change-amount")
			.log("Change amount for: ${body}!")
			.process(new Processor() {
				
				@Override
				public void process(Exchange exchange) throws Exception {
					HashMap<String, Object> body = exchange.getIn().getBody(HashMap.class);
					
					body.put("amount", 526);
					exchange.getOut().setBody(body, HashMap.class);
				}
			})
			.to("log:org.camunda.demo.route.DemoRoute?level=INFO&showAll=true&multiline=true")
			.onException(RuntimeException.class)
		        .maximumRedeliveries(5)
		        .maximumRedeliveryDelay(10000)
		        .redeliveryDelay(1000)
		        .useOriginalMessage()
		        .end() // onException
			.to("camunda-bpm://message?messageName=changed-amount");
		
		from("direct:check-amount")
			.log("Check amount for: ${body}!");
	}

}
