package org.camunda.demo.route;

import org.apache.camel.CamelContext;
import org.camunda.bpm.camel.spring.CamelServiceImpl;
import org.camunda.bpm.engine.ProcessEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CamelConfiguration {
	
	private Logger logger = LoggerFactory.getLogger(CamelConfiguration.class.getName());
	
	@Autowired
	ProcessEngine processEngine;
	
	@Autowired
	CamelContext camelContext;
	
	@Bean(name="camel")
	public CamelServiceImpl camel() {
		
		logger.info("CamelServiceImpl initialization started !!!!!!!!!!!!!!!!!!!!!!");
		
		CamelServiceImpl camelServiceImpl = new CamelServiceImpl();
		camelServiceImpl.setCamelContext(camelContext);
		camelServiceImpl.setProcessEngine(processEngine);
		
		logger.info("CamelServiceImpl initialization finished !!!!!!!!!!!!!!!!!!!!!!");
		
		return camelServiceImpl;
	}
}
