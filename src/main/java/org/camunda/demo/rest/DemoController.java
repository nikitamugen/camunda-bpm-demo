package org.camunda.demo.rest;

import java.util.Map;

import org.camunda.demo.service.RunProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {
	
	@Autowired
	private RunProcessService runProcessService;
	
	@RequestMapping("/run/{id}/process")
    public void greeting(
    		@PathVariable("id") String processId,
    		@RequestParam Map<String, String> variables) {
		runProcessService.run(processId, variables);
    }
}
